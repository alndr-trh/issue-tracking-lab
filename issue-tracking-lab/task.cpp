#include <iostream>
#include <iomanip>
#include <random>

const int STRING_SIZE = 20;
typedef char String[STRING_SIZE];

const int MINIMUM_NUMBER_OF_WORKERS = 5, MAXIMUM_NUMBER_OF_WORKERS = 50, FIRST_HIRE_YEAR = 2013,
LAST_HIRE_YEAR = 2018, FIRST_HIRE_MONTH = 1, LAST_HIRE_MONTH = 7, MINIMUM_HIRE_MONTH = 1,
MAXIMUM_HIRE_MONTH = 12, MINIMUM_HIRE_DAY = 1, FIRST_HIRE_DAY = 1, LAST_HIRE_DAY = 15,
NUMBER_OF_MONTHS = 12, MAX_DAYS_IN_MONTHS[NUMBER_OF_MONTHS] = { 31, 28, 31, 30, 31, 30, 31, 31, 30,
31, 30, 31 };

struct Worker
{
	int hireYear;
	int hireMonth;
	int hireDay;
	Worker(int _hireYear = 0, int _hireMonth = 0, int _hireDay = 0)
	{
		hireYear = _hireYear;
		hireMonth = _hireMonth;
		hireDay = _hireDay;
	}
};

void generateWorkersDates(Worker*, int&);
void writeMinMaxHireDates(Worker*, const int&);
void writeWorkersExpOnCertainDate(Worker*, const int&);

int main()
{
	Worker workers[MAXIMUM_NUMBER_OF_WORKERS];
	int workersCount = 0;
	generateWorkersDates(workers, workersCount);
	writeMinMaxHireDates(workers, workersCount);
	writeWorkersExpOnCertainDate(workers, workersCount);
	system("pause");
	return 0;
}

void generateWorkersDates(Worker *_workers, int &_workersCount)
{
	int workersCount;
	std::random_device rd;
	std::mt19937 gn(rd());
	std::uniform_int_distribution<int> workersRange(MINIMUM_NUMBER_OF_WORKERS, MAXIMUM_NUMBER_OF_WORKERS);
	std::uniform_int_distribution<int> yearsRange(FIRST_HIRE_YEAR, LAST_HIRE_YEAR);
	std::uniform_int_distribution<int> monthsRange(MINIMUM_HIRE_MONTH, MAXIMUM_HIRE_MONTH);
	std::uniform_int_distribution<int> daysJanuaryRange(MINIMUM_HIRE_DAY, MAX_DAYS_IN_MONTHS[0]);
	std::uniform_int_distribution<int> daysFebruaryLYRange(MINIMUM_HIRE_DAY, (MAX_DAYS_IN_MONTHS[1] + 1));
	std::uniform_int_distribution<int> daysFebruaryRange(MINIMUM_HIRE_DAY, MAX_DAYS_IN_MONTHS[1]);
	std::uniform_int_distribution<int> daysMarchRange(MINIMUM_HIRE_DAY, MAX_DAYS_IN_MONTHS[2]);
	std::uniform_int_distribution<int> daysAprilRange(MINIMUM_HIRE_DAY, MAX_DAYS_IN_MONTHS[3]);
	std::uniform_int_distribution<int> daysMayRange(MINIMUM_HIRE_DAY, MAX_DAYS_IN_MONTHS[4]);
	std::uniform_int_distribution<int> daysJunyRange(MINIMUM_HIRE_DAY, MAX_DAYS_IN_MONTHS[5]);
	std::uniform_int_distribution<int> daysJulyRange(MINIMUM_HIRE_DAY, MAX_DAYS_IN_MONTHS[6]);
	std::uniform_int_distribution<int> daysAugustRange(MINIMUM_HIRE_DAY, MAX_DAYS_IN_MONTHS[7]);
	std::uniform_int_distribution<int> daysSeptemberRange(MINIMUM_HIRE_DAY, MAX_DAYS_IN_MONTHS[8]);
	std::uniform_int_distribution<int> daysOctoberRange(MINIMUM_HIRE_DAY, MAX_DAYS_IN_MONTHS[9]);
	std::uniform_int_distribution<int> daysNovemberRange(MINIMUM_HIRE_DAY, MAX_DAYS_IN_MONTHS[10]);
	std::uniform_int_distribution<int> daysDecemberRange(MINIMUM_HIRE_DAY, MAX_DAYS_IN_MONTHS[11]);
	std::uniform_int_distribution<int> daysLYRange(MINIMUM_HIRE_DAY, LAST_HIRE_DAY);
	workersCount = workersRange(gn);
	_workersCount = workersCount;
	for (int i = 0; i < workersCount; i++) {
		_workers[i].hireYear = yearsRange(gn);
		if ((_workers[i].hireYear != LAST_HIRE_YEAR) && (_workers[i].hireMonth != LAST_HIRE_MONTH)) {
			_workers[i].hireMonth = monthsRange(gn);
			switch (_workers[i].hireMonth)
			{
			case 1:
				_workers[i].hireDay = daysJanuaryRange(gn);
				break;
			case 2:
				if ((_workers[i].hireYear % 4) == 0) {
					_workers[i].hireDay = daysFebruaryLYRange(gn);
				}
				else {
					_workers[i].hireDay = daysFebruaryRange(gn);
				}
				break;
			case 3:
				_workers[i].hireDay = daysMarchRange(gn);
				break;
			case 4:
				_workers[i].hireDay = daysAprilRange(gn);
				break;
			case 5:
				_workers[i].hireDay = daysMayRange(gn);
				break;
			case 6:
				_workers[i].hireDay = daysJunyRange(gn);
				break;
			case 7:
				_workers[i].hireDay = daysJulyRange(gn);
				break;
			case 8:
				_workers[i].hireDay = daysAugustRange(gn);
				break;
			case 9:
				_workers[i].hireDay = daysSeptemberRange(gn);
				break;
			case 10:
				_workers[i].hireDay = daysOctoberRange(gn);
				break;
			case 11:
				_workers[i].hireDay = daysNovemberRange(gn);
				break;
			case 12:
				_workers[i].hireDay = daysDecemberRange(gn);
				break;
			}
		}
		else {
			_workers[i].hireMonth = monthsRange(gn);
			_workers[i].hireDay = daysLYRange(gn);
		}
		std::cout << "Worker " << i + 1 << " - " << _workers[i].hireYear << '-';
		(_workers[i].hireMonth < 10) ? (std::cout << '0' << _workers[i].hireMonth << '-') : (std::cout << _workers[i].hireMonth << '-');
		(_workers[i].hireDay < 10) ? (std::cout << '0' << _workers[i].hireDay << std::endl) : (std::cout << _workers[i].hireDay << std::endl);
	}
	return;
}

void writeMinMaxHireDates(Worker* _workers, const int& _NUMBER_OF_WORKERS)
{
	int minHireYear = LAST_HIRE_YEAR, maxHireYear = FIRST_HIRE_YEAR,
		minHireMonth = MAXIMUM_HIRE_MONTH, maxHireMonth = MINIMUM_HIRE_MONTH,
		minHireDay = MAX_DAYS_IN_MONTHS[0], maxHireDay = MINIMUM_HIRE_DAY;
	bool isDateLower = false, isDateBigger = false;
	for (int i = 0; i < _NUMBER_OF_WORKERS; i++) {
		isDateLower = isDateBigger = false;
		isDateLower = (minHireDay > _workers[i].hireDay && minHireMonth == _workers[i].hireMonth
			&& minHireYear == _workers[i].hireYear) || (minHireMonth > _workers[i].hireMonth
				&& minHireYear == _workers[i].hireYear) || (minHireYear > _workers[i].hireYear);
		isDateBigger = (maxHireDay < _workers[i].hireDay && maxHireMonth == _workers[i].hireMonth
			&& maxHireYear == _workers[i].hireYear) || (maxHireMonth < _workers[i].hireMonth
				&& maxHireYear == _workers[i].hireYear) || (maxHireYear < _workers[i].hireYear);
		if (isDateLower) {
			minHireDay = _workers[i].hireDay;
			minHireMonth = _workers[i].hireMonth;
			minHireYear = _workers[i].hireYear;
		}
		if (isDateBigger) {
			maxHireDay = _workers[i].hireDay;
			maxHireMonth = _workers[i].hireMonth;
			maxHireYear = _workers[i].hireYear;
		}
	}
	std::cout << "\nMinimum hire date: " << minHireYear << '-';
	if (minHireMonth < 10) {
		std::cout << '0';
	}
	std::cout << minHireMonth << '-';
	if (minHireDay < 10) {
		std::cout << '0';
	}
	std::cout << minHireDay;
	std::cout << "\nMaximum hire date: " << maxHireYear << '-';
	if (maxHireMonth < 10) {
		std::cout << '0';
	}
	std::cout << maxHireMonth << '-';
	if (maxHireDay < 10) {
		std::cout << '0';
	}
	std::cout << maxHireDay << std::endl;
	return;
}

void writeWorkersExpOnCertainDate(Worker* _workers, const int& _NUMBER_OF_WORKERS)
{
	bool isDateCorrect = false, isStringDirty = false;
	String date = "\0";
	char year[5] = "\0", month[3] = "\0", day[3] = "\0";
	int yearNumber = 0, monthNumber = 0, dayNumber = 0, workerExp = 0;
	while (!isDateCorrect) {
		isStringDirty = false;
		std::cout << "\nWrite date in a correct form and in acceptable range (YYYY-MM-DD): ";
		std::cin.getline(date, STRING_SIZE);
		for (int i = 0; i < strlen(date) && !isStringDirty; i++) {
			isStringDirty = (i != 4 && i != 7 && !isdigit(date[i])) ||
				((i == 4 || i == 7) && date[i] != '-') || strlen(date) > 10;
		}
		if (!isStringDirty) {
			strncpy_s(year, date, 4);
			strncpy_s(month, date + 5, 2);
			strncpy_s(day, date + 8, 2);
			yearNumber = atoi(year);
			monthNumber = atoi(month);
			dayNumber = atoi(day);
			isDateCorrect = yearNumber >= FIRST_HIRE_YEAR && yearNumber <= LAST_HIRE_YEAR &&
				monthNumber >= MINIMUM_HIRE_MONTH && monthNumber <= MAXIMUM_HIRE_MONTH &&
				dayNumber >= MINIMUM_HIRE_DAY && dayNumber <= MAX_DAYS_IN_MONTHS[monthNumber + 1];
			if (!isDateCorrect) {
				std::wcout << "Date is in unacceptable range. Try again.\n";
			}
		}
		else {
			std::cout << "Date entered in incorrect form. Try again.\n";
		}
	}
	std::cout << "Workers experience:\n";
	for (int i = 0; i < _NUMBER_OF_WORKERS; i++) {
		if ((_workers[i].hireMonth == monthNumber && _workers[i].hireDay <= dayNumber) ||
			_workers[i].hireMonth < monthNumber) {
			(yearNumber - _workers[i].hireYear <= 0) ? (workerExp = 0) :
				(workerExp = yearNumber - _workers[i].hireYear);
		}
		else {
			(yearNumber - _workers[i].hireYear <= 0) ? (workerExp = 0) :
				(workerExp = yearNumber - _workers[i].hireYear - 1);
		}
		std::cout << "Worker " << i + 1 << " - " << workerExp;
		(workerExp == 1) ? (std::cout << " year\n") : (std::cout << " years\n");
	}
	return;
}